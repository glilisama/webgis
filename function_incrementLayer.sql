CREATE OR REPLACE FUNCTION incrementer_layer(layer_name TEXT, object_id INT)
RETURNS TEXT AS $$
BEGIN
  -- Vérifier si le type de topping existe déjà dans la table "layer"
  IF NOT EXISTS (SELECT 1 FROM label WHERE name = layer_name) THEN
    -- Insérer le nouveau type de layer dans la table "layer"
    INSERT INTO label (name) VALUES (layer_name);
  END IF;

  -- Insérer les liens entre le gâteau et les toppings dans la table "object_label"
  INSERT INTO object_label (object_id, label_id)
  VALUES (object_id, (SELECT id FROM label WHERE name = layer_name));
  RETURN 'layer incremented successfully.';
END;
$$ LANGUAGE plpgsql;