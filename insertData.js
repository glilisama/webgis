const { Pool } = require('pg');

const pool = new Pool({
  user: 'lilisama',
  host: 'localhost',
  database: 'mapping',
  port: 5432,
})


const express = require('express');
const app = express();
app.use(express.json());


// Check if the layer name already exists in the `label` table and return the corresponding id

async function insertLayer(name) { 
  const query = 'SELECT id FROM label WHERE name = $1';
  const result = await pool.query(query, [name]);
  
  if (result.rows.length > 0) {
    // The label already exists, return its id
    return result.rows[0].id;
  } else {
   // The layer does not exist, insert it into the `label` table and return the new id
    const insertQuery = 'INSERT INTO label (name) VALUES ($1) RETURNING id';
    const insertResult = await pool.query(insertQuery, [name]);
    return insertResult.rows[0].id;
  }
}


// insert a object in the table with empty description and return the database id

async function createObject() {
  try {
    // Execute the SQL query to insert the row with empty "info" column
    const result = await pool.query('INSERT INTO object(description) VALUES (\'\') RETURNING id');

    // Retrieves generated ID from query result
    const id = result.rows[0].id;

    return id;
  } catch (error) {
    console.error('Error while inserting into array object:', error);
    throw error;
  }
}

// insert a object which linked with its layers 

async function insertObjLayer(object_id, layer_id) {
  try {
    // Exécute la requête SQL pour insérer la ligne avec les données "type" et "info"
    await pool.query('INSERT INTO object_label(object_id, label_id) VALUES ($1, $2)', [object_id, layer_id]);
  } catch (error) {
    console.error('Error while inserting into array object_label', error);
    throw error;
  }
}


// Check if the drawing name already exists in the `drawing` table and return the corresponding id

async function insertDrawing(name) {
  const query = 'SELECT id FROM drawing WHERE name = $1';
  const result = await pool.query(query, [name]);
  
  if (result.rows.length > 0) {
    // The drawing already exists, return its id
    return result.rows[0].id;
  } else {
   // The drawing does not exist, insert it into the `drawing` table and return the new id
    const insertQuery = 'INSERT INTO drawing(name) VALUES ($1) RETURNING id';
    const insertResult = await pool.query(insertQuery, [name]);
    return insertResult.rows[0].id;
  }
}

// insert an element in id_mapping

async function insertMapping(autocad_id, drawing_id, database_id) {
  try {
    // Execute the SQL query to insert the row with "autocad_id", "drawing_id", "database_id" data
    await pool.query('INSERT INTO id_mapping(autocad_id, drawing_id, database_id) VALUES ($1, $2, $3)', [autocad_id, drawing_id, database_id]);
  } catch (error) {
    console.error('Error while inserting into array id_ mapping', error);
    throw error;
  }
}

// to get user permission

async function getPermissionByUserId(userId) {
  try {
    // Execute the SQL query to retrieve the permission corresponding to the user_id
    const result = await pool.query('SELECT permission FROM label_user WHERE user_id = $1', [userId]);

    if (result.rows.length > 0) {
      const permission = result.rows[0].permission;
      return permission;
    } else {
      return null; // No permissions found for the given user_id
    }
  } catch (error) {
    console.error('Error retrieving permission from label_user table:', error);
    throw error;
  }
}


//inserting into the table commit

async function insertIntoCommit(userId, comment) {
  try {
    // Execute the SQL query to insert the row with "userId" and "comment" data
    const insertQuery='INSERT INTO commit(user_id, comment) VALUES ($1, $2)RETURNING id'; 
    const insertResult = await pool.query(insertQuery,[userId, comment]);
    return insertResult.rows[0].id;
  } catch (error) {
    console.error('Error while inserting into array commit', error);
    throw error;
  }
}

//inserting circle into the table geom

async function insertCircleIntoGeom(x_center, y_center, radius, objectId, commitId, autocad) {
  try {
    const result = await pool.query('select circleToPolyline($1, $2, $3) as polyline', [x_center, y_center, radius]);
    const geom_data = result.rows[0].polyline;
  // Execute the SQL query to insert the row with "userId" and "comment" data
    await pool.query('INSERT INTO geom(object_id, data, commit_id, autocad_data) VALUES ($1, $2, $3, $4)', [objectId, geom_data, commitId, autocad]);
  } catch (error) {
    console.error('Error while inserting circle into array geom', error);
    throw error;
  }
}

//inserting arc into the table geom

async function insertArcIntoGeom(x_center, y_center, start_angle, end_angle, radius, objectId, commitId, autocad) {
  try {
    const result = await pool.query('select arcToPolyline($1, $2, $3, $4, $5) as polyline', [radius, start_angle, end_angle, x_center, y_center]);
    const geom_data = result.rows[0].polyline;
    // Execute the SQL query to insert the row with "userId" and "comment" data
    await pool.query('INSERT INTO geom(object_id, data, commit_id, autocad_data) VALUES ($1, $2, $3, $4)', [objectId, geom_data, commitId, autocad]);
  } catch (error) {
    console.error('Error while inserting arc into array geom', error);
    throw error;
  }
}

//inserting ellipse into the table geom

async function insertElliIntoGeom(startAngle, endAngle, radius, x_major, y_major, x_center, y_center, objectId, commitId, autocad){
  try {
    const result = await pool.query('select elliToPolyline($1, $2, $3, $4, $5, $6, $7) as polyline', [startAngle, endAngle, radius, x_major, y_major, x_center, y_center]);
    const geom_data = result.rows[0].polyline;
    // Execute the SQL query to insert the row with "userId" and "comment" data
    await pool.query('INSERT INTO geom(object_id, data, commit_id, autocad_data) VALUES ($1, $2, $3, $4)', [objectId, geom_data, commitId, autocad]);
  } catch (error) {
    console.error('Error while inserting ellipse into array geom', error);
    throw error;
  }
}

//inserting point into the table geom

async function insertPointIntoGeom(x_center, y_center, objectId, commitId, autocad){
  try {
    const result = await pool.query('select createPoint($1, $2) as point', [x_center, y_center]);
    const geom_data = result.rows[0].point;
    // Execute the SQL query to insert the row with "userId" and "comment" data
    await pool.query('INSERT INTO geom(object_id, data, commit_id, autocad_data) VALUES ($1, $2, $3, $4)', [objectId, geom_data, commitId, autocad]);
  } catch (error) {
    console.error('Error while inserting point into array geom', error);
    throw error;
  }
}

//inserting line into the table geom

async function insertLineIntoGeom(x_center1, y_center1, x_center2, y_center2, objectId, commitId, autocad){
  try {
    const result = await pool.query('select createPoint($1, $2, $3, $4) as line', [x_center1, y_center1, x_center2, y_center2]);
    const geom_data = result.rows[0].line;
    // Execute the SQL query to insert the row with "userId" and "comment" data
    await pool.query('INSERT INTO geom(object_id, data, commit_id, autocad_data) VALUES ($1, $2, $3, $4)', [objectId, geom_data, commitId, autocad]);
  } catch (error) {
    console.error('Error while inserting line into array geom', error);
    throw error;
  }
}

//inserting polyline into the table geom

async function insertPolyIntoGeom(coords, objectId, commitId, autocad){
  try {
    const result = await pool.query('select createPolyline($1::numeric[][]) as polyline', [coords]);
    const geom_data = result.rows[0].polyline;
    // Execute the SQL query to insert the row with "userId" and "comment" data
    await pool.query('INSERT INTO geom(object_id, data, commit_id, autocad_data) VALUES ($1, $2, $3, $4)', [objectId, geom_data, commitId, autocad]);
  } catch (error) {
    console.error('Error while inserting line into array geom', error);
    throw error;
  }
}

app.post('/insertData', async (req, res) => {
  const data = req.body;
  const userId = data.userId;
  const comment = data.comment;
  const drawing_name = data.drawing_name;
 
  const drawingId = await insertDrawing(drawing_name);

  const permission = await getPermissionByUserId(userId);
  console.log(permission);
  const objects = data.objects;
  
  
  if(permission != 'r'){
    if(comment == '') {
      console.log("please provide comment");
    }else{
      const commitId = await insertIntoCommit(userId, comment);
      objects.forEach(async (obj) => {
        const className = obj.className;
        const AutocadId = obj.id;
        const layer = obj.layer;
      
        try {
        // queries 
          
          const layerId = await insertLayer(layer);
          const objectId = await createObject();

          await insertObjLayer(objectId, layerId);
          await insertMapping(AutocadId, drawingId, objectId);
          if(className=='AcDbArc'){
            const x_center = obj.coordscenter.x;
            const y_center = obj.coordscenter.y;
            const start_angle = obj.coordsRadAng.startAngle;
            const end_angle = obj.coordsRadAng.endAngle;
            const radius = obj.coordsRadAng.radius;
            await insertArcIntoGeom(x_center, y_center, start_angle, end_angle, radius, objectId, commitId, className)
          } else 
          if(className=='AcDbEllipse'){
            const x_center = obj.coordscenter.x;
            const y_center = obj.coordscenter.y;
            const startAngle = obj.coordsRadAng.startAngle;
            const endAngle = obj.coordsRadAng.endAngle;
            const radius = obj.coordsRadAng.radius;
            const x_major = obj.coordsMajorAxis.x;
            const y_major = obj.coordsMajorAxis.y;
            await insertElliIntoGeom(startAngle, endAngle, radius, x_major, y_major, x_center, y_center, objectId, commitId, autocad);
          } else 
          if(className=='AcDbCircle'){
            const x_center = obj.coordscenter.x;
            const y_center = obj.coordscenter.y;
            const radius = obj.coordsRad.radius;
            await insertCircleIntoGeom(x_center, y_center, radius, objectId, commitId, autocad);
          } else 
          if(className=='AcDbPoint'){
            const x_center = obj.coords.x;
            const y_center = obj.coords.y;
            await insertPointIntoGeom(x_center, y_center, objectId, commitId, autocad);
          }else 
          if(className=='AcDbLine'){
            const x_center1 = obj.coords.x1;
            const y_center1 = obj.coords.y1;
            const x_center2 = obj.coords.x2;
            const y_center2 = obj.coords.y2;
            await insertLineIntoGeom(x_center1, y_center1, x_center2, y_center2, objectId, commitId, autocad);
          }else{
            const coords = obj.coords;

            const array = [];
            coords.forEach(async (c) => {
            
              const row = [];
              row.push(c.x);
              row.push(c.y);
              array.push(row);
            
            });
            await insertPolyIntoGeom(array, objectId, commitId, autocad);
          }
          res.status(200).json({ message: 'Data uploaded successfully' });
        } catch (error) {
          console.error('An error has occurred:', error);
        } 
      });
    }
  } else {
    res.status(200).json({ message: 'You are not allowed to write in the data !' });
  }
});


/*app.post('/insertData', async (req, res) => {
  const data = req.body;
  console.log(data);
  const objects = data.objects;
  const user_id = data.userId;
  const comment = data.comment;
 
  console.log(comment);
  

  res.status(200).json({ message: 'Data uploaded successfully' });
});*/




/*app.post('/insertData', async (req, res) => {
  const data = req.body;
  const coords = data.coords;
  console.log(coords.length);
  const array = [];
  coords.forEach(async (c) => {
    const row = [];
              row.push(c.x);
              row.push(c.y);
              array.push(row);
    });
    console.log(array);
    const result = await pool.query('select createPolyline($1::numeric[][]) as polyline', [array]);
    const geom_data = result.rows[0].polyline;
    console.log(geom_data);
  res.status(200).json({ message: 'Data uploaded successfully' });
});*/

app.listen(3000, () => {
  console.log('Serveur en écoute sur le port 3000');
});