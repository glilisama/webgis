CREATE OR REPLACE FUNCTION circleToPolyline(x_center numeric, y_center numeric, radius numeric)
  RETURNS geometry(LineString) AS
$$
DECLARE
  center_point geometry(Point) := ST_MakePoint(x_center, y_center);
  num_points integer := 100; -- Adjust the number of points according to the desired precision
BEGIN
  RETURN (
    WITH circle_points AS (
      SELECT (ST_DumpPoints(ST_MakeLine(
        ARRAY(SELECT ST_Translate(center_point, radius * cos((2 * pi() * i) / num_points), radius * sin((2 * pi() * i) / num_points)))
      ))).geom AS point
      FROM generate_series(0, num_points - 1) AS i
    )
    SELECT ST_MakeLine(circle_points.point)
    FROM circle_points
  );
END;
$$
LANGUAGE plpgsql;
