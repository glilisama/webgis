--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.3 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';


--
-- Name: rights; Type: TYPE; Schema: public; Owner: lilisama
--

CREATE TYPE public.rights AS ENUM (
    'r',
    'w',
    'r/w'
);


ALTER TYPE public.rights OWNER TO lilisama;

--
-- Name: arctopolyline(numeric, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.arctopolyline(radius numeric, start_angle numeric, end_angle numeric, x_center numeric, y_center numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
  center_point geometry(Point) := ST_MakePoint(x_center, y_center);
  start_point geometry(Point) := ST_Translate(center_point, radius * cos(start_angle), radius * sin(start_angle));
  end_point geometry(Point) := ST_Translate(center_point, radius * cos(end_angle), radius * sin(end_angle));
BEGIN
  RETURN ST_MakeLine(ARRAY[start_point, end_point]);
END;
$$;


ALTER FUNCTION public.arctopolyline(radius numeric, start_angle numeric, end_angle numeric, x_center numeric, y_center numeric) OWNER TO lilisama;

--
-- Name: circletopolyline(numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.circletopolyline(x_center numeric, y_center numeric, radius numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
  center_point geometry(Point) := ST_MakePoint(x_center, y_center);
  num_points integer := 100; -- Ajustez le nombre de points selon la précision souhaitée
BEGIN
  RETURN (
    WITH circle_points AS (
      SELECT (ST_DumpPoints(ST_MakeLine(
        ARRAY(SELECT ST_Translate(center_point, radius * cos((2 * pi() * i) / num_points), radius * sin((2 * pi() * i) / num_points)))
      ))).geom AS point
      FROM generate_series(0, num_points - 1) AS i
    )
    SELECT ST_MakeLine(circle_points.point)
    FROM circle_points
  );
END;
$$;


ALTER FUNCTION public.circletopolyline(x_center numeric, y_center numeric, radius numeric) OWNER TO lilisama;

--
-- Name: createline(numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.createline(x1 numeric, y1 numeric, x2 numeric, y2 numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN ST_MakeLine(ST_MakePoint(x1, y1), ST_MakePoint(x2, y2));
END;
$$;


ALTER FUNCTION public.createline(x1 numeric, y1 numeric, x2 numeric, y2 numeric) OWNER TO lilisama;

--
-- Name: createpoint(numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.createpoint(x numeric, y numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN ST_MakePoint(x, y);
END;
$$;


ALTER FUNCTION public.createpoint(x numeric, y numeric) OWNER TO lilisama;

--
-- Name: createpolyline(numeric[]); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.createpolyline(coords numeric[]) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
  points geometry(Point)[];
  i integer;
BEGIN
  FOR i IN 1..array_length(coords, 1) LOOP
    points := array_append(points, ST_MakePoint(coords[i][1], coords[i][2]));
  END LOOP;

  RETURN ST_MakeLine(points);
END;
$$;


ALTER FUNCTION public.createpolyline(coords numeric[]) OWNER TO lilisama;

--
-- Name: ellitopolyline(numeric, numeric, numeric, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.ellitopolyline(startangle numeric, endangle numeric, radius numeric, x_major numeric, y_major numeric, x_center numeric, y_center numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
  num_points integer := 100; -- Vous pouvez ajuster ce nombre selon la précision souhaitée
  start_point geometry(Point) := ST_Translate(ST_Rotate(ST_Translate(ST_MakePoint(radius, 0), x_center, y_center), startAngle), x_center, y_center);
  end_point geometry(Point) := ST_Translate(ST_Rotate(ST_Translate(ST_MakePoint(radius, 0), x_center, y_center), endAngle), x_center, y_center);
BEGIN
  RETURN (
    SELECT ST_MakeLine(ARRAY(
      SELECT (ST_DumpPoints(point)).geom
      FROM (
        SELECT ST_Translate(ST_Rotate(ST_Translate(ST_MakePoint(radius * cos((2 * pi() * i) / num_points), radius * sin((2 * pi() * i) / num_points)), x_center, y_center), startAngle), x_center, y_center) AS point
        FROM generate_series(0, num_points) AS i
      ) AS ellipse_points
    ))
  );
END;
$$;


ALTER FUNCTION public.ellitopolyline(startangle numeric, endangle numeric, radius numeric, x_major numeric, y_major numeric, x_center numeric, y_center numeric) OWNER TO lilisama;

--
-- Name: ellitopolyline(numeric, numeric, numeric, numeric, numeric, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.ellitopolyline(startangle numeric, endangle numeric, radius numeric, x_major numeric, y_major numeric, z_major numeric, x_center numeric, y_center numeric, z_center numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
  center_point geometry(Point) := ST_MakePoint(x_center, y_center, z_center);
  major_axis_point geometry(Point) := ST_MakePoint(x_major, y_major, z_major);
  num_points integer := 100; -- Ajustez le nombre de points selon la précision souhaitée
BEGIN
  RETURN (
    WITH ellipse_points AS (
      SELECT (ST_Translate(ST_Rotate(ST_Translate(ST_Scale(major_axis_point, radius, radius, 1), x_center, y_center, z_center), radians(startAngle)), x_center, y_center, z_center)).geom AS point
      FROM generate_series(0, num_points) AS i
      WHERE (2 * pi() * i) / num_points <= radians(endAngle)
    )
    SELECT ST_MakeLine(ellipse_points.point)
    FROM ellipse_points
  );
END;
$$;


ALTER FUNCTION public.ellitopolyline(startangle numeric, endangle numeric, radius numeric, x_major numeric, y_major numeric, z_major numeric, x_center numeric, y_center numeric, z_center numeric) OWNER TO lilisama;

--
-- Name: incrementer_layer(text, integer); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.incrementer_layer(layer_name text, object_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN
  -- Vérifier si le type de topping existe déjà dans la table "layer"
  IF NOT EXISTS (SELECT 1 FROM label WHERE name = layer_name) THEN
    -- Insérer le nouveau type de layer dans la table "layer"
    INSERT INTO label (name) VALUES (layer_name);
  END IF;

  -- Insérer les liens entre le gâteau et les toppings dans la table "object_label"
  INSERT INTO object_label (object_id, label_id)
  VALUES (object_id, (SELECT id FROM label WHERE name = layer_name));
  RETURN 'layer incremented successfully.';
END;
$$;


ALTER FUNCTION public.incrementer_layer(layer_name text, object_id integer) OWNER TO lilisama;

--
-- Name: topolyline(numeric, numeric, numeric, numeric, numeric, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: lilisama
--

CREATE FUNCTION public.topolyline(startangle numeric, endangle numeric, radius numeric, x_major numeric, y_major numeric, z_major numeric, x_center numeric, y_center numeric, z_center numeric) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
  center_point geometry(Point) := ST_MakePoint(x_center, y_center, z_center);
  major_axis_point geometry(Point) := ST_MakePoint(x_major, y_major, z_major);
  num_points integer := 100; -- Ajustez le nombre de points selon la précision souhaitée
BEGIN
  RETURN (
    WITH ellipse_points AS (
      SELECT (ST_Translate(ST_Rotate(ST_Translate(ST_Scale(major_axis_point, radius, radius, 1), x_center, y_center, z_center), radians(startAngle)), x_center, y_center, z_center)).geom AS point
      FROM generate_series(0, num_points) AS i
      WHERE (2 * pi() * i) / num_points <= radians(endAngle)
    )
    SELECT ST_MakeLine(ellipse_points.point)
    FROM ellipse_points
  );
END;
$$;


ALTER FUNCTION public.topolyline(startangle numeric, endangle numeric, radius numeric, x_major numeric, y_major numeric, z_major numeric, x_center numeric, y_center numeric, z_center numeric) OWNER TO lilisama;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: commit; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.commit (
    id integer NOT NULL,
    user_id integer,
    comment character varying(100),
    "timestamp" timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.commit OWNER TO lilisama;

--
-- Name: commit_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.commit_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commit_id_seq OWNER TO lilisama;

--
-- Name: commit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.commit_id_seq OWNED BY public.commit.id;


--
-- Name: drawing; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.drawing (
    id integer NOT NULL,
    name character varying(50)
);


ALTER TABLE public.drawing OWNER TO lilisama;

--
-- Name: drawing_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.drawing_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drawing_id_seq OWNER TO lilisama;

--
-- Name: drawing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.drawing_id_seq OWNED BY public.drawing.id;


--
-- Name: geom; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.geom (
    id integer NOT NULL,
    object_id integer,
    data public.geometry,
    commit_id integer,
    autocad_data character varying(100)
);


ALTER TABLE public.geom OWNER TO lilisama;

--
-- Name: geom_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.geom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.geom_id_seq OWNER TO lilisama;

--
-- Name: geom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.geom_id_seq OWNED BY public.geom.id;


--
-- Name: id_mapping; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.id_mapping (
    id integer NOT NULL,
    autocad_id bigint,
    drawing_id integer,
    database_id integer
);


ALTER TABLE public.id_mapping OWNER TO lilisama;

--
-- Name: id_mapping_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.id_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_mapping_id_seq OWNER TO lilisama;

--
-- Name: id_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.id_mapping_id_seq OWNED BY public.id_mapping.id;


--
-- Name: label; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.label (
    id integer NOT NULL,
    name character varying(50)
);


ALTER TABLE public.label OWNER TO lilisama;

--
-- Name: label_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.label_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.label_id_seq OWNER TO lilisama;

--
-- Name: label_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.label_id_seq OWNED BY public.label.id;


--
-- Name: label_user; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.label_user (
    id integer NOT NULL,
    user_id integer,
    label_id integer,
    permission public.rights
);


ALTER TABLE public.label_user OWNER TO lilisama;

--
-- Name: label_users_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.label_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.label_users_id_seq OWNER TO lilisama;

--
-- Name: label_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.label_users_id_seq OWNED BY public.label_user.id;


--
-- Name: object; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.object (
    id integer NOT NULL,
    description character varying(100)
);


ALTER TABLE public.object OWNER TO lilisama;

--
-- Name: object_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.object_id_seq OWNER TO lilisama;

--
-- Name: object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.object_id_seq OWNED BY public.object.id;


--
-- Name: object_label; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.object_label (
    id integer NOT NULL,
    object_id integer,
    label_id integer
);


ALTER TABLE public.object_label OWNER TO lilisama;

--
-- Name: object_label_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.object_label_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.object_label_id_seq OWNER TO lilisama;

--
-- Name: object_label_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.object_label_id_seq OWNED BY public.object_label.id;


--
-- Name: usr; Type: TABLE; Schema: public; Owner: lilisama
--

CREATE TABLE public.usr (
    id integer NOT NULL,
    name character varying(50),
    email character varying(50),
    password character varying(50)
);


ALTER TABLE public.usr OWNER TO lilisama;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: lilisama
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO lilisama;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lilisama
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.usr.id;


--
-- Name: commit id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.commit ALTER COLUMN id SET DEFAULT nextval('public.commit_id_seq'::regclass);


--
-- Name: drawing id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.drawing ALTER COLUMN id SET DEFAULT nextval('public.drawing_id_seq'::regclass);


--
-- Name: geom id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.geom ALTER COLUMN id SET DEFAULT nextval('public.geom_id_seq'::regclass);


--
-- Name: id_mapping id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.id_mapping ALTER COLUMN id SET DEFAULT nextval('public.id_mapping_id_seq'::regclass);


--
-- Name: label id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.label ALTER COLUMN id SET DEFAULT nextval('public.label_id_seq'::regclass);


--
-- Name: label_user id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.label_user ALTER COLUMN id SET DEFAULT nextval('public.label_users_id_seq'::regclass);


--
-- Name: object id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.object ALTER COLUMN id SET DEFAULT nextval('public.object_id_seq'::regclass);


--
-- Name: object_label id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.object_label ALTER COLUMN id SET DEFAULT nextval('public.object_label_id_seq'::regclass);


--
-- Name: usr id; Type: DEFAULT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.usr ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: commit; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.commit (id, user_id, comment, "timestamp") FROM stdin;
1	1	testing comment	2023-06-30 13:06:26.312741
2	1	testing commit	2023-06-30 15:24:47.623159
3	1	testing commit	2023-06-30 15:26:45.540239
\.


--
-- Data for Name: drawing; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.drawing (id, name) FROM stdin;
1	drawing1
\.


--
-- Data for Name: geom; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.geom (id, object_id, data, commit_id, autocad_data) FROM stdin;
1	\N	0106000000010000000103000000010000001100000049AA329650E11041F3281E6481952541353963B9223906413DC5001CF82A254148B9C8DD27010341E69A2F3A27173041D720BB47AF6902414186261B94103141409A9ECCA21BF540D2107CF913B93141C7A9563FBFF2F840E48CAA7052CB3241F8CB2F54F3800341AEB812155B9833411456F69DAE600B417CEC9C45368833410D65EB28DF4A104146E77A1B845A3441D8869B0A4F281041BCC0076989F33441C1E98BEE6649134174D4BB3F24393541F8B76F297A7D18415D4EE7B16400344166D84B8C42311A411224331F351532412304C84B57071541B290941249032F41CC2455F1DDF3124102008BB42FDA2E4111DBCC9E647A124185827C0366AB254149AA329650E11041F3281E6481952541	\N	1
2	\N	010200000064000000333333333333154000000000000000403F8FF42B882C15409CF5DF0A5DA80140B88CDBD28D181540F37A1C580D4F0340867A025758F71440DCEA3BDB65F2044056A6964509C91440C49462E8BE9006404296F267CF8D14400A5D5DE0752808404B225994E64514405A5F92D7EEB709404A2F817197F1134076452C36963D0B40581F2F2D379113403EDCDB4FE2B70C40FE1F26262725134089F093F154250E401E48C889D4AD1240C09DADE37C840F4050E9C8E5B72B12401D7777AFFB691040898560AE549F1140D4767DB938091140D4767DB938091140898560AE549F11401E7777AFFB69104050E9C8E5B72B1240C29DADE37C840F401E48C889D4AD124088F093F154250E40FE1F2626272513403DDCDB4FE2B70C40581F2F2D3791134076452C36963D0B404A2F817197F113405B5F92D7EEB709404A225994E64514400A5D5DE0752808404296F267CF8D1440C39462E8BE90064056A6964509C91440DDEA3BDB65F20440867A025758F71440F57A1C580D4F0340B78CDBD28D1815409DF5DF0A5DA801403F8FF42B882C154000000000000000403333333333331540C81440EA45AFFC3F3F8FF42B882C15401C0AC74FE561F93FB88CDBD28D181540482A8849341BF63F867A025758F7144078D63A2F82DEF23F56A6964509C91440E08B8A7E285EEF3F4396F267CF8D14409E82B6A14420E93F4B225994E645144024EA4E27A709E33F492F817197F11340041E2181ED40DA3F581F2F2D3791134058F7C0E6B0AACD3FFD1F26262725134020909814C7E0AE3F1E48C889D4AD124060C7DDDDEB7EBABF50E9C8E5B72B1240506DD7978B93D0BF888560AE549F1140805808E64AF5D9BFD6767DB938091140804A472EBF5DE1BF1E7777AFFB691040F440424EA46EE5BFC29DADE37C840F40E4FF30313929E9BF8CF093F154250E40BCFA7869B989ECBF3EDCDB4FE2B70C40447A098CBB8CEFBF78452C36963D0B402A8964519A17F1BF5C5F92D7EEB709400A59CA9F3D37F2BF0B5D5DE07528084056995A162524F3BFC79462E8BE90064018EA095C61DDF3BFDCEA3BDB65F20440DC326E4B3762F4BFF57A1C580D4F0340FC3CD2AF20B2F4BF9DF5DF0A5DA80140CCCCCCCCCCCCF4BF0100000000000040FC3CD2AF20B2F4BFC91440EA45AFFC3FDE326E4B3762F4BF1A0AC74FE561F93F16EA095C61DDF3BF462A8849341BF63F58995A162524F3BF7CD63A2F82DEF23F0C59CA9F3D37F2BFDC8B8A7E285EEF3F2C8964519A17F1BF9882B6A14420E93F507A098CBB8CEFBF32EA4E27A709E33FBCFA7869B989ECBF081E2181ED40DA3FF0FF30313929E9BF88F7C0E6B0AACD3F0041424EA46EE5BFC0909814C7E0AE3F844A472EBF5DE1BF40C7DDDDEB7EBABFA05808E64AF5D9BF386DD7978B93D0BF486DD7978B93D0BF985808E64AF5D9BF20C7DDDDEB7EBABF884A472EBF5DE1BFA08F9814C7E0AE3FF440424EA46EE5BF98F7C0E6B0AACD3FF4FF30313929E9BF101E2181ED40DA3FBCFA7869B989ECBF34EA4E27A709E33F547A098CBB8CEFBF9C82B6A14420E93F2C8964519A17F1BFD28B8A7E285EEF3F0A59CA9F3D37F2BF7ED63A2F82DEF23F58995A162524F3BF482A8849341BF63F18EA095C61DDF3BF150AC74FE561F93FDC326E4B3762F4BFCB1440EA45AFFC3FFC3CD2AF20B2F4BFFDFFFFFFFFFFFF3FCCCCCCCCCCCCF4BF98F5DF0A5DA80140FC3CD2AF20B2F4BFF37A1C580D4F0340DE326E4B3762F4BFDAEA3BDB65F2044018EA095C61DDF3BFC59462E8BE90064058995A162524F3BF095D5DE0752808400C59CA9F3D37F2BF565F92D7EEB709402E8964519A17F1BF70452C36963D0B40587A098CBB8CEFBF42DCDB4FE2B70C40B8FA7869B989ECBF8AF093F154250E40E8FF30313929E9BFBF9DADE37C840F40F840424EA46EE5BF1C7777AFFB6910408C4A472EBF5DE1BFD2767DB938091140B85808E64AF5D9BF888560AE549F1140586DD7978B93D0BF50E9C8E5B72B124040C7DDDDEB7EBABF1E48C889D4AD1240808F9814C7E0AE3FFC1F26262725134038F7C0E6B0AACD3F561F2F2D37911340E01D2181ED40DA3F482F817197F113401CEA4E27A709E33F4B225994E64514409A82B6A14420E93F4296F267CF8D1440D08B8A7E285EEF3F56A6964509C9144070D63A2F82DEF23F867A025758F71440472A8849341BF63FB78CDBD28D181540140AC74FE561F93F3F8FF42B882C1540CA1440EA45AFFC3F	\N	\N
\.


--
-- Data for Name: id_mapping; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.id_mapping (id, autocad_id, drawing_id, database_id) FROM stdin;
1	1453960150128	1	9
2	1453960150128	1	10
3	1453960150128	1	11
4	1453960150144	1	12
5	1453960150128	1	13
\.


--
-- Data for Name: label; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.label (id, name) FROM stdin;
1	Tuyen MongDuong 2017
3	Tuyen MongDuong 2016
\.


--
-- Data for Name: label_user; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.label_user (id, user_id, label_id, permission) FROM stdin;
1	2	1	r
2	1	1	r/w
3	3	3	w
\.


--
-- Data for Name: object; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.object (id, description) FROM stdin;
1	
2	
3	
4	
5	
6	
7	
8	
9	
10	
11	
12	
13	
\.


--
-- Data for Name: object_label; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.object_label (id, object_id, label_id) FROM stdin;
1	4	1
2	5	1
3	6	1
4	7	1
5	8	1
6	9	1
7	10	1
8	11	1
9	12	1
10	13	1
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: usr; Type: TABLE DATA; Schema: public; Owner: lilisama
--

COPY public.usr (id, name, email, password) FROM stdin;
1	gitlover12	gitlover@gmail.com	123456
2	coal24	coal@gmail.com	654321
3	destructor23	destructor@gmail.com	1234567
\.


--
-- Name: commit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.commit_id_seq', 3, true);


--
-- Name: drawing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.drawing_id_seq', 1, true);


--
-- Name: geom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.geom_id_seq', 2, true);


--
-- Name: id_mapping_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.id_mapping_id_seq', 5, true);


--
-- Name: label_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.label_id_seq', 3, true);


--
-- Name: label_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.label_users_id_seq', 4, true);


--
-- Name: object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.object_id_seq', 13, true);


--
-- Name: object_label_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.object_label_id_seq', 10, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lilisama
--

SELECT pg_catalog.setval('public.users_id_seq', 3, true);


--
-- Name: commit commit_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.commit
    ADD CONSTRAINT commit_pkey PRIMARY KEY (id);


--
-- Name: drawing drawing_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.drawing
    ADD CONSTRAINT drawing_pkey PRIMARY KEY (id);


--
-- Name: geom geom_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.geom
    ADD CONSTRAINT geom_pkey PRIMARY KEY (id);


--
-- Name: id_mapping id_mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.id_mapping
    ADD CONSTRAINT id_mapping_pkey PRIMARY KEY (id);


--
-- Name: label label_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.label
    ADD CONSTRAINT label_pkey PRIMARY KEY (id);


--
-- Name: label_user label_users_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.label_user
    ADD CONSTRAINT label_users_pkey PRIMARY KEY (id);


--
-- Name: object_label object_label_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.object_label
    ADD CONSTRAINT object_label_pkey PRIMARY KEY (id);


--
-- Name: object object_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.object
    ADD CONSTRAINT object_pkey PRIMARY KEY (id);


--
-- Name: usr users_pkey; Type: CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.usr
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: commit commit_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.commit
    ADD CONSTRAINT commit_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.usr(id);


--
-- Name: geom geom_commit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.geom
    ADD CONSTRAINT geom_commit_id_fkey FOREIGN KEY (commit_id) REFERENCES public.commit(id);


--
-- Name: geom geom_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.geom
    ADD CONSTRAINT geom_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object(id);


--
-- Name: id_mapping id_mapping_database_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.id_mapping
    ADD CONSTRAINT id_mapping_database_id_fkey FOREIGN KEY (database_id) REFERENCES public.object(id);


--
-- Name: id_mapping id_mapping_drawing_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.id_mapping
    ADD CONSTRAINT id_mapping_drawing_id_fkey FOREIGN KEY (drawing_id) REFERENCES public.drawing(id);


--
-- Name: label_user label_users_label_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.label_user
    ADD CONSTRAINT label_users_label_id_fkey FOREIGN KEY (label_id) REFERENCES public.label(id);


--
-- Name: label_user label_users_users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.label_user
    ADD CONSTRAINT label_users_users_id_fkey FOREIGN KEY (user_id) REFERENCES public.usr(id);


--
-- Name: object_label object_label_label_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.object_label
    ADD CONSTRAINT object_label_label_id_fkey FOREIGN KEY (label_id) REFERENCES public.label(id);


--
-- Name: object_label object_label_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lilisama
--

ALTER TABLE ONLY public.object_label
    ADD CONSTRAINT object_label_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object(id);


--
-- PostgreSQL database dump complete
--

