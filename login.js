const { Pool } = require('pg');

const pool = new Pool({
  user: 'lilisama',
  host: 'localhost',
  database: 'mapping',
  port: 5432,
})


const express = require('express');
const app = express();
app.use(express.json());




app.post('/login', (req, res) => {
    const user = req.body;
    const email = user.email;
    const password = user.password;

    // check if the email exist
    const emailExistsQuery = 'SELECT * FROM usr WHERE email = $1';
    pool.query(emailExistsQuery, [email], (err, result) => {
        if (result.rowCount === 0) {
        res.status(401).json({ message: 'Email not found' });
        } else {
        // check if the password is correct
        const passwordCheckQuery = 'SELECT * FROM usr WHERE email = $1 AND password = $2';
        pool.query(passwordCheckQuery, [email, password], (err, result) => {
            if (result.rowCount > 0) {
                res.json({ message: 'Connexion succeed' });
            } else {
                res.status(401).json({ message: 'incorrect password' });
            }  
        });
        }
    });
});

/*app.post('/login', (req, res) => {
    const user = req.body;
    const email = user.email;
    const password = user.password;
    console.log(user);
    console.log(email);
    console.log(password);
});*/

app.listen(3000, () => {
    console.log('Serveur en écoute sur le port 3000');
  });
  